/* 【例3-5】求解简单的四则运算表达式。输入一个形式如"操作数 运算符 操作数"的四则运算表达式，输出运算结果。 */

/*求解简单的四则运算表达式 */
#include <stdio.h>
int main (void)
{
    double value1, value2;
    char op;

    printf ("Type in an expression: ");                 /* 提示输入一个表达式 */
    scanf ("%lf%c%lf", &value1, &op, &value2);    /* 输入表达式 */

    if (op == '+')                                /* 判断运算符是否为 '+' */
        printf ("=%.2f\n", value1 + value2);            /* 对操作数做加法操作*/
    else if (op == '-')                           /* 否则判断运算符是否为 '-' */
        printf ("=%.2f\n", value1 - value2);
    else if (op == '*')                           /* 否则判断运算符是否为 '-' */
        printf ("=%.2f\n", value1 * value2);
    else if (op == '/')                           /* 否则判断运算符是否为 '-' */
        printf ("=%.2f\n", value1 / value2);
    else                                               /* 运算符输入错误 */
        printf ("Unknown operator\n");

    return 0;
}
