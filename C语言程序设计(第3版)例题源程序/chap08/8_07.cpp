/*【例8-7】输入10个整数作为数组元素，分别使用数组和指针来计算并输出它们的和。*/

/*  分别使用数组和指针计算数组元素之和 */
#include <stdio.h>
int main(void)
{
	int i, a[10], *p;
	long sum = 0;

	printf("Enter 10 integers: ");
	for(i = 0; i < 10; i++) 
		scanf("%d", &a[i]);

	for ( i = 0; i < 10; i++)
		sum = sum + a[i];
	printf("calculated by array, sum=%ld \n", sum);

	sum=0; 							/* 重新初始化sum为0 */
	for(p = a; p <= a+9; p++)       /* 使用指针求和 */
		sum = sum + *p;
	printf("calculated by pointer , sum=%ld \n", sum);

	return 0;
}    
